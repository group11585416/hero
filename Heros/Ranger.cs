﻿using HeroProject.Exceptions;
using HeroProject.Item;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HeroProject.Item.EnumClass;

namespace HeroProject.Heros
{
    public class Ranger : Hero
    {


      

            /// <summary>
            /// Constructor sets the correct value in HeroAttribute when created also generate validateWeapontypes list and ValidateArmorTypeList
            /// </summary>
            /// <param name="name"></param>
            public Ranger(string name) : base(name)
            {
                //validWeapontype = new List<weapontype

                //set the attribute level to 1
                HeroAttribute = new HeroAttribute(7, 1, 1);
                ValidateWeaponTypes = new List<WeaponType> { WeaponType.Bows };
                ValidateArmorTypes = new List<ArmorType> { ArmorType.Leather,ArmorType.Mail};

            }


            /// <summary>
            /// Method for leveling up, sets Level+=1 heroattribute. Strengt+=1,Dexerity+=1,Intelligence+=5 each time it is called
            /// </summary>

            public override void LevelUp()
            {
                Level += 1;
                HeroAttribute.Strength += 1;
                HeroAttribute.Dexerity += 5;
                HeroAttribute.Intelligence += 1;

            }

            /// <summary>
            /// Iterates through validateWeaponTypes to check if this weapon is allowed for this hero.also checks Level is above requred level of this weapon. If both are OK
            ///the weapon is aded to euipment list with slot weapon. If not both are OK InvalidWeaponException is thrown and writes error message in console
            /// </summary>
            /// <param name="weapon"></param>

            public override void EquipWeapon(Weapon weapon)
            {


                try
                {
                    foreach (var item in ValidateWeaponTypes)
                    {

                        if (item == weapon.WeaponType && weapon.RequiredLevel <= Level)
                        {
                            Equipment.Add(slot.Weapon, weapon);
                            return;
                        }
                    }

                    throw new InvalidWeaponException();
                }
                catch (InvalidWeaponException ex)
                {

                    Console.WriteLine(ex);
                }



            }
            /// <summary>
            /// Iterates through validateArmorTypes to check if this armor is allowed for this hero.also checks Level is above requred level of this armor. If both are OK
            ///the armor is aded to euipment diconary with the clot placement. If not both are OK InvalidArmorException is thrown and writes error message in console
            /// </summary>
            /// <param name="armor"></param>
            /// <param name="place"></param>
            public override void EquipArmor(Armor armor, slot place)
            {
                try
                {
                    foreach (var item in ValidateArmorTypes)
                    {
                        if (item == armor.ArmorType && armor.RequiredLevel <= Level)
                        {
                            Equipment.Add(place, armor);
                            return;
                        }

                    }
                    throw new InvalidArmorException();
                }
                catch (InvalidArmorException ex)
                {

                    Console.WriteLine(ex);
                }

            }


            /// <summary>
            /// adds bonusAttribute from armor to Heroattribute.Intellicence/strength and dexerity
            /// </summary>
            /// <returns></returns>
            public override int Totalattribute()
            {

                int totalAttribute = HeroAttribute.Intelligence + HeroAttribute.Strength + HeroAttribute.Dexerity;
                int armorattribute = 0;

                foreach (var item in Equipment.Select(x => x.Value).OfType<Armor>())
                {

                    armorattribute += item.BonusAttribute;

                }
                return totalAttribute + armorattribute;

            }

            /// <summary>
            /// Calculates weapon damage: Iterate trough equipement diconary to find weapon and returns weapon.Wapondamage * (1f + HeroAttribute.Dexerity / 100f);
            /// </summary>
            /// <returns></returns>
            public override double Damage()
            {
                double damage = 1;


                foreach (var item in Equipment.Select(x => x.Value).OfType<Weapon>())
                {

                    damage = item.Wapondamage * (1f + HeroAttribute.Dexerity / 100f);

                    return damage;
                }

                return damage;


            }

            /// <summary>
            /// Writes to console: HeroType,Name, Level, Heroattribute.strength,Dexerity,Intelligence, and Damage
            /// </summary>
            /// <returns></returns>
            public override string Display()
            {
                {
                    //Create object of stringbuilerclass here (Append format)
                    StringBuilder sb = new StringBuilder();

                    sb.AppendFormat("Name: {0}\nClass: {1}\nLevel: {2} \nStrength: {3}\nDexerity: {4}\n Intelligence: {5}\nDamage: {6}",
                        Name, this.GetType().Name, Level, HeroAttribute.Strength, HeroAttribute.Dexerity, HeroAttribute.Intelligence, Damage());

                    string test = sb.ToString();
                    return test;
                    //Allthe detail of the hero should be display here
                }
            }








        }
}
