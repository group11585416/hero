﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace HeroProject.Heros
{
    public class HeroAttribute
    {

        public int Dexerity { get; set; }
        public int Strength { get; set; }
        public int Intelligence { get; set; }

        public HeroAttribute(int dexerity, int strength, int intelligence)
        {
            this.Dexerity = dexerity;
            this.Strength = strength;
            this.Intelligence = intelligence;

        }

        // public static HeroAttribute
        //do addition of two heroAttributes using plus

        /// <summary>
        /// a method for HeroAttribute to add two instances and return the sum (Example: strength of two instances
        /// </summary>
        /// <param name="inputHero"></param>
        /// <returns></returns>
        public int addTwoInsancesStrength(HeroAttribute inputHero)
        {
            return inputHero.Strength + Strength;

        }

    }
}
