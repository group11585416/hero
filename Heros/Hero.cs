﻿using HeroProject.Item;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using static HeroProject.Item.EnumClass;

namespace HeroProject.Heros
{
    public abstract class Hero
    {

        public int Level { get; set; }
        public string Name { get; set; }
        public int LevelAttribute { get; set; }
        
        public HeroAttribute HeroAttribute { get; set; }
        public Dictionary<slot,Items> Equipment { get; set; }

        public List<WeaponType> ValidateWeaponTypes { get; set; }
       
         public List<ArmorType> ValidateArmorTypes { get; set; }

        public int DamagingAttribute { get; set; }
        //prop equioment
        //prop valid weapomType
        //prop valid ArmorType

        public Hero(string name)
        {
            Level = 1;
            this.Name = name;

            Equipment = new Dictionary<EnumClass.slot, Items>();

        }

        public abstract int Totalattribute();
        

        
        public virtual void LevelUp()
        {
            
        }
        public virtual void EquipWeapon(Weapon weapon)
        {

        }
        public virtual void EquipArmor(Armor armor, slot place)
        {

        }
        public abstract double Damage();





        public abstract string Display();
       
    //  public void EquipArmor(Armor armor)
    //  {
            //Equip armor if it is a leagel armor type for the charackrter. if not leagal, then you have to throw invalidArmorExceptions
    //  }

    //  public HeroAttribute TotalAttribute()
    //  {
            //Calculate total Attribute here
    //  }
    }
}
