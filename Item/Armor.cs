﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HeroProject.Item.EnumClass;

namespace HeroProject.Item
{

    public class Armor : Items
    {
        public int BonusAttribute { get; set; }
       
        public ArmorType ArmorType { get; set; }


        

        public Armor(int bonusAttribute, int requiredLevel, ArmorType armorType,string name)
        {
            BonusAttribute = bonusAttribute;
            RequiredLevel = requiredLevel;
            ArmorType = armorType;
            Name = name;
        }

        // tell type of armor - set the property for that
        // tell property for bonus attributes the armor gives hero
        // sett the public consturctor for armor (armor type, name,, armor attribute,slot, required level
    }
}
