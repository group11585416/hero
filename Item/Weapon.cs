﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HeroProject.Item.EnumClass;

namespace HeroProject.Item
{
    public class Weapon : Items
    {


        public int Wapondamage { get; set; }

        public WeaponType WeaponType { get; set; }

      
        public Weapon(int requiredLevel, int wapondamage, WeaponType weaponType,string name)
        {
            RequiredLevel = requiredLevel;
            Wapondamage = wapondamage;
            WeaponType = weaponType;
            Slot = slot.Weapon;
            Name = name;
        }
    }


}
