﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace HeroProject.Exceptions
{
    public class InvalidArmorException: Exception
    {
        public override string Message
        {
            get
            {
                return "This Armor is not suiteable for you";
            }
        }

    }
}
