﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroProject.Exceptions
{
    public class InvalidWeaponException:Exception
    {

        public override string Message
        {
            get
            {
                return "This Weapon is not suiteable for you";
            }
        }


    }
}
